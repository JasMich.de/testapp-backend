# Installation
***Note: these instructions are in alpha state. Some parts may be wrong or different than described***

TestApp requires several repositorys. For an overview, please refer to https://gitlab.com/testapp-system/

## Dependencies and System
TestApp is testet with the current Ubuntu LTS release. Most other Linuxes as well as macOS and FreeBSD should work finely as well. Windows might not be working correctly.

It is tested using Apache 2, PHP-FPM 7.2 (but 7.3 is working as well).

For good performance, you should calculate about **1GB of memory per 500 users**.

### Ubuntu
```
sudo apt install php7.2 php7.2-fpm mariadb-server php7.2-mysql
sudo systemctl enable php-fpm && sudo systemctl start php-fpm
sudo a2enmod php-fpm
```

### General
TestApp requires HTTPS. ***Many functions won't work using HTTP***. The easiest way for setting up HTTPS is using `certbot`.

We highly recommend not to use standard PHP-CGI but PHP-FPM. For this, please ensure that the `php7.2-fpm` package is installed. Afterwards, run `sudo a2enmod php-fpm` to enable PHP in Apache.

## Databases
TestApp requires one empty database *or* the name for a database to install. All tables will be created during the installation process.

## Set up the source
1. Create a virtual host for your testapp instance, e.g. `testapp.myschool.ga` with it's root at `/var/www/testapp-frontend`

2. Create two www-folders, one for the frontend and one for the backend:
```
sudo mkdir /var/www/testapp-frontend /var/www/testapp-backend
```

3. Make sure you have writing access as non-superuser:
```
sudo chown $USER:$USER  /var/www/{testapp-frontend, testapp-backend}
```

4. Move to the frondend's folder and download the source:
```
cd /var/www/testapp-frontend
gut clone https://gitlab.com/testapp-system/testapp-frontend . # Don't forget the point at the end

cd /var/www/testapp-backend
gut clone https://gitlab.com/testapp-system/testapp-backend . # Don't forget the point at the end
```

5. Link the backend into the frontend:
```
sudo ln -s /var/www/testapp-backend /var/www/testapp-frontend/api
```
Note: *You can change this URL by changing the file `functions.js` in frontend repo. Use the first few lines for customization.*

6. Change the ownership back to `www-data` (on some systems only `www`)
```
sudo chown www-data:www-data  /var/www/{testapp-frontend, testapp-backend}
```

## Install the database
Perform the installation
```
DBUSER="mydbusername"; DBPASS="secretpass"; DBNAME="testappdb"; curl https://testapp.myschool.ga/api/?install&user=${DBUSER}&pass=${DBPASS}&table=${DBNAME}&host=localhost
```

## Mailing
We highly recommend using the TestApp mailing service. For that, please configure the file `mail.inc.php` in your backend.

## Clients
To use mobile and desktop clients, you need to customize their source to work with your Domain. Otherwise, you could use a paied subscription on our TestApp servers.
